

## Initial observations and assumptions

- The original URL has changed to https://completion.amazon.com/api/2017/suggestions?mid=ATVPDKIKX0DER&alias=aps&suggestion-type=KEYWORD&prefix=
- This can be observed in the network tab of the browser inspector as show below

![](src/main/resources/static/amz_sugg_1.png)



- It is also observed that for each character in the keyword provided, Amazon provides a different set of suggestions.

- The correctness of the ranking of the suggestions is hinted at in the `refTag` key of the suggestions object in each `autocompletion` suggestion.

  ```
  "refTag": "nb_sb_ss_i_1_2"
  ```

- In the above ref tag it can be confirmed from the last 2 pieces of data (`"_1_2"`) that the recommended keyword is ranked first from second set of suggestions on to the entire keyword entered by the user (e.g. If they keyword is "Samsung", the second set of suggestions will be provided for the autocompletion "Sa" and the first suggestion from this set on is generally observed to be "Samsung Galaxy  10").

  

## Correctness of hint

- As observed by matching  the 10 suggestions recommended on the Amazon website search box with the 10 suggestions of the **Autocompletion API** request,  it can be confirmed that the suggestions retrieved are sorted in order of importance. Therefore, the hint provided is correct.



## Algorithm + Implementation

- We iterate over each character in the keyword entered by the user and build  a prefix set  **(e.g. s, sa, sam, ...., samsung).**

![](src/main/resources/static/amz_sugg_2.png)

- For each iteration in this set, we retrieve a set of keyword suggestions making a rest call to the Amazon **Autocompletion API**.
- No results will be returned for empty strings or keywords with no suggestions. The score returned for such a request will be 0.

- We set up a `cumulative score` as an `AtomicInteger` to ensure thread safety during concurrent usage.
- We iterate through each `autocompletion` and retrieve the total number of suggestions that match with the keyword.
- We then add the `initial score calculation` for the keyword for each iteration to the `cumulative score`. The initial score is calculated by using the following variables: the `total count of suggestions` in each `autocompletion`, the `maximum possible score` (100), the `keyword index` within the list of suggestions
- The formula for this calculation is as follows:

```java
initial keyword score = maximum possible score * ( ( total count of suggestions - keyword index) / total count of suggestions )
```

- The  `cumulative score` is then divided by the `total count of autocompletions`.

- To ensure quicker response time, we set up callable functions with futures, and set up a thread pool of 10.

- To ensure that we meet the SLA of 10 seconds, we set up a timeout of 9 seconds.
- If the total response time for the entire request exceed 9 seconds, we return an keyword object with a score of 0. The resulting TimeoutException is then logged.



## Precision of outcome

When we tested they keywords "samsung" and "iphone charger", we received the following results:

- `Samsung` returned a score of 71

![request_1](src/main/resources/static/request_1.png)

- `Iphone charger` returned a score of 50

![request_2](src/main/resources/static/request_2.png)

- When we entered a random string with no results, it returned a score of 0.

![request_2](src/main/resources/static/request_3.png)



The results have been consistently between 0 and 100 from the outcome. While there is no guarantee that the retrieved score is precise, the calculation considers all possible parameters to determine a score with a higher chance of correctness. The size of the string and presence of high value keywords have also shown to have a direct impact on the score.

For example, `apple iphone charger` returns a score of 70 as opposed to `iphone charger` returning a score of 50. This is due the addition of the word `apple ` which has a high value.



