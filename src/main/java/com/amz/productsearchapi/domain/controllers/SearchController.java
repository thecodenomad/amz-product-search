package com.amz.productsearchapi.domain.controllers;

import com.amz.productsearchapi.domain.models.KeywordScore;
import com.amz.productsearchapi.domain.services.KeywordScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {

    @Autowired
    private KeywordScoreService keywordScoreService;

    @GetMapping(value = "/estimate", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<KeywordScore> retrieveKeywordScore(@RequestParam String keyword) {
        return ResponseEntity.ok(keywordScoreService.getKeywordScore(keyword));
    }

}
