package com.amz.productsearchapi.domain.services;

import com.amz.productsearchapi.domain.models.Autocompletion;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Log4j2
public class AutocompletionService {

    @Autowired
    private RestClientService restClientService;

    /**
     * Returns entire set of keywords for each contiguous character set in a keyword
     * e.g. For Samsung, ["s", "sa", "sam", ... "samsung"] and returns a list of autocompletion suggestions
     * @param keyword keyword searched for by user
     * @return autocompletion keywords
     */
    public List<Autocompletion> getAutocompletionSuggestedKeywordList(String keyword) {
        List<String> autocompletionKeywords = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        List<String> keywordChars = Arrays.stream(keyword.split("")).collect(Collectors.toList());

        for (String character : keywordChars) {
            if (!StringUtils.isEmpty(character)) {
                sb.append(character);
                autocompletionKeywords.add(sb.toString());
            }
        }

        if (!StringUtils.isEmpty(keyword)) {
            return autocompletionKeywords
                    .stream()
                    .map(i -> {
                        try {
                            return restClientService.retrieveSearchResultsFromAmazon(i);
                        } catch (Exception ex) {
                            log.warn("Received exception: {}", ex.getMessage(), ex);
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

}
