package com.amz.productsearchapi.domain.services;

import com.amz.productsearchapi.domain.models.Autocompletion;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;


@Service
@Log4j2
public class RestClientService {
    @Value("${amazon.search.autocomplete.url}")
    String amazonProductSearchUrl;
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * Makes a rest call to the Amazon Autocomplete API to retrieve the autocompletion suggestion
     * @param keyword
     * @return autocompletion suggestion
     */
    public Autocompletion retrieveSearchResultsFromAmazon(String keyword) {
        try {
            return restTemplate.getForObject(amazonProductSearchUrl + keyword, Autocompletion.class);
        } catch(IllegalArgumentException ex) {
            log.warn("Illegal argument exception: {}", ex.getMessage(), ex);
        } catch(ResourceAccessException ex) {
            log.warn("Resource access exception: {}", ex.getMessage(), ex);
        }

        return null;
    }
}
