package com.amz.productsearchapi.domain.services;

import com.amz.productsearchapi.domain.models.Autocompletion;
import com.amz.productsearchapi.domain.models.KeywordScore;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

@Service
@Log4j2
public class KeywordScoreService {

    @Value("${request.timeout.inSeconds}")
    private Integer requestTimeoutInSeconds;

    @Value("${threadpool.size}")
    private Integer threadPoolCount;

    @Autowired
    private AutocompletionService autocompletionService;

    /**
     * Gets the score for the searched keyword
     * @param keyword keyword seacrhed for by user
     * @return keyword score
     */
    public KeywordScore getKeywordScore(String keyword) {
        ExecutorService executorService = Executors.newFixedThreadPool(threadPoolCount);
        Callable<KeywordScore> keywordScoreCallable = () -> {
            KeywordScore keywordScore = new KeywordScore();
            keywordScore.setKeyword(keyword);
            if (StringUtils.isEmpty(keyword)) {
                keywordScore.setScore(0);
            } else {
                keywordScore.setScore(calculateKeywordScore(keyword));
            }

            return keywordScore;

        };

        Future<KeywordScore> keywordScoreFuture = executorService.submit(keywordScoreCallable);

        try {
            return keywordScoreFuture.get(requestTimeoutInSeconds, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            log.warn("Received interrupted exception: {}", ex.getMessage(), ex);
        } catch (ExecutionException ex) {
            log.warn("Received execution exception: {}", ex.getMessage(), ex);
        } catch (TimeoutException ex) {
            log.warn("Received timeout exception: {}", ex.getMessage(), ex);
        } finally {
            keywordScoreFuture.cancel(true);
        }

        return new KeywordScore(keyword, 0);

    }

    /**
     * Calculates the final keyword score by determining the ranking of the suggestion within Amazon's
     * autocompletion index across all possible combinations of the keyword
     * @param keyword keyword searched for by user
     * @return keyword score
     */
    public Integer calculateKeywordScore(String keyword) {
        AtomicInteger cumulativeScore = new AtomicInteger(0);

        List<Autocompletion> autocompletionSuggestedKeywordList = autocompletionService.getAutocompletionSuggestedKeywordList(keyword);
        for (Autocompletion autocompletionSuggestion : autocompletionSuggestedKeywordList) {
            int keywordSuggestionCount = autocompletionSuggestion.getSuggestions().size();
            IntStream.range(0, keywordSuggestionCount)
                    .filter(index -> autocompletionSuggestion.getSuggestions().get(index).getValue().startsWith(keyword))
                    .findFirst()
                    .ifPresent(index ->
                            cumulativeScore.set(cumulativeScore.get() + calculateInitialScore(keywordSuggestionCount, index)));
        }

        return cumulativeScore.get() / autocompletionSuggestedKeywordList.size();
    }

    /**
     * Calculates score based on maximum score value, keyword suggestion count and keyword suggestion index
     * @param keywordSuggestionCount suggestion count for keyword
     * @param keywordSuggestionIndex ranked index of keyword
     * @return initially calculated score
     */
    private Integer calculateInitialScore(Integer keywordSuggestionCount, Integer keywordSuggestionIndex) {
        int maximumAllowedScore = 100;
        return (maximumAllowedScore * ((keywordSuggestionCount - keywordSuggestionIndex) / keywordSuggestionCount));
    }
}
