package com.amz.productsearchapi.domain.models;

import com.amz.productsearchapi.domain.models.Suggestion;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Autocompletion {
    private String alias;
    private String prefix;
    private String suffix;
    private List<Suggestion> suggestions;
    private String responseId;
}
