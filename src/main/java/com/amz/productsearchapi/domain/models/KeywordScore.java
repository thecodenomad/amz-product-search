package com.amz.productsearchapi.domain.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeywordScore {
    private String keyword;
    private Integer score;
}
