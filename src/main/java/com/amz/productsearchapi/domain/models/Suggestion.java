package com.amz.productsearchapi.domain.models;

import lombok.Data;

import java.util.List;

@Data
public class Suggestion {
    private String suggType;
    private String type;
    private String value;
    private String refTag;
    private List<Scope> scopes;
}
