package com.amz.productsearchapi.domain.models;

import lombok.Data;

@Data
public class Scope {
    private String type;
    private String value;
    private String display;
}
